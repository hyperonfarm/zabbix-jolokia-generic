#!/usr/bin/env bash

BIN_PATH="/srv/jolokia/bin"
TOMCAT_APPS="$(find /data/tomcat -maxdepth 1 |cut -d'/' -f4|grep -v "^$")"
DEFAULTS_PATH="/etc/default"
JOPORT="13501"
JOHOST="127.0.0.1"
JOLOKIA_URL="$(wget -qO- https://jolokia.org/download.html|grep jolokia-jvm|grep -o '<a .*href=.*>'|sed -e 's/<a /\n<a /g'|sed -e 's/<a .*href=['"'"'"]//' -e 's/["'"'"'].*$//' -e '/^$/ d')"

[[ -d $BIN_PATH ]] || mkdir -p $BIN_PATH
[[ -f $BIN_PATH/jolokia-jvm.jar ]] || wget -q $JOLOKIA_URL -O $BIN_PATH/jolokia-jvm.jar

for tapp in $TOMCAT_APPS; do
  sed -i '/jolokia/d' $DEFAULTS_PATH/$tapp
  echo "JAVA_OPTS=\"\$JAVA_OPTS -javaagent:/srv/jolokia/bin/jolokia-jvm.jar=port=$JOPORT,host=$JOHOST\"" >> $DEFAULTS_PATH/$tapp
  JOPORT=$(( $JOPORT + 1 ))
done
