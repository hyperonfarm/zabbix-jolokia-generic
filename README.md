# zabbix-jolokia-generic

This project is designed to facilitate relatively easy and lightweight JVM monitoring using Jolokia JMX-HTTP bridge.
It does not require any daemons or cron tasks, because all checks are executed from zabbix agent.